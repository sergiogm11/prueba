<html>
   <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<script>
 // Write on keyup event of keyword input element
 $(document).ready(function(){
 $("#search").keyup(function(){
 _this = this;
 // Show only matching TR, hide rest of them
 $.each($("#mytable tbody tr"), function() {
 if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
 $(this).hide();
 else
 $(this).show();
 });
 });
});
</script>
  <style>
body {
    margin: 0;
    padding: 0;
}


ul {
    margin: 0;
    padding: 0;
}

ul li {
    margin-left: auto;
    margin-right: auto;
}

    table, th, tr, td {
        border: solid 0.5px #BADED3;    
        border-collapse: collapse;
        text-align: center;
    }

    .container-fluid {
      margin-right: 7%;
     margin-left: 7%;

    }
.hv:hover {
        background-color: #8288DF;
        color:white;
        font-weight: bold;
        transform: scale(1.05);
    }

    th{
        font-size: 24px;
        height: 65px;

        background-color: #5359B6;
        color:white;
    }
    td{
        width: 75px;
        height: 65px;
        font-size: 18px;
    }
    tr:nth-child(odd){
        background-color: #DCFCFF;
        transition: background-color .5s
        font-weight .2s;
        font-weight: normal;
    }
    .nav-link{
        color:white;
    }
</style>
</head>
<body class="bg-secondary">
  <nav class="navbar navbar-info navbar-expand-sm bg-dark fixed-top">
  <div class="container-fluid">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="http://localhost:8080/codeigniter/index.php/homeController">Inicio</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="http://localhost:8080/codeigniter/index.php/alumnesController">Listado Alumnos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="http://localhost:8080/codeigniter/index.php/gruposController/">Listado Grupos</a>
      </li>
    </ul>
  </div>

      <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ms-auto">
      <li>
         <input type="text" class="form-control pull-right" style="width:300px; margin-right: 140px;" id="search" placeholder="Busca elementos en la tabla...">

      </li>
        </ul>
    </div>
</nav>
<br><br>
<br>

<table id="myTable" style="width:85%; margin: auto; background-color: #BAF2F6")>
        <thead class="table-warning">
          <tr>
        <th>Grupo</th>
        <th>Nombre</th>
        <th>Alumnos de grupo</th>
      </tr>
          </thead>

<?php foreach ($grupos as $grup) :?>


  <tr class="hv">
    <td>    <?= $grup->codigo ?></td>
    <td> <?= $grup->nombre ?> </td>
    <td><a href="http://localhost:8080/codeigniter/index.php/alumnesController/alumnosgrupo/<?=$grup->codigo?>"> Alumnos </a> </td>
  </tr>

</body>
<?php endforeach; ?>
</table>
    </html>
