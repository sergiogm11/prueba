<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
         <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>

        <title>FORMULARIO</title>
    </head>

    <style>
            .form-group {
                margin-left: 40%;
                margin-bottom: 10px;
            }
            .boton{
                margin-left: 48%;
            }
            .form-control:invalid{
            color:red;    
            }
       
    </style>
 <?php if (!empty(\Config\Services::validation()->getErrors())):?>
<div class="alert alert-danger" role="alert">
    <?= \Config\Services::validation()->listErrors(); ?>
</div>
<?php endif; ?>
    
<?= form_open(site_url('formularioController/insertar'),['class'=>"needs-validation"]) ?>
    <br>

  <div class="form-group row">
	<!--<label for="NIA" class="col-2 col-form-label">NIA</label>-->
        <div class="col-md-4">
            <span id="NIA">NIA</span>       
            <?= form_input('NIA',set_value('NIA'),['placeholder'=>"Pon tu NIA",  'required'=>"required", 'class'=>"form-control", 'maxlength' => '8',])?>
	</div>
  </div>
 
<div class="form-group row">
	<div class="col-4">
            <span id="nif">NIF o DNI</span>
	<?= form_input('nif',set_value('nif'),['placeholder'=>"NIF o DNI", 'required'=>"required", 'class'=>"form-control", 'maxlength' => '9' ])?>
	</div>
           
  </div>

  <div class="form-group row">
	<div class="col-4">
            <span id="nombre">Nombre</span>
  	<?= form_input('nombre',set_value('nombre'),['placeholder'=>"Nombre", 'required'=>"required", 'class'=>"form-control" ])?>
	</div>
  </div>
 
 
 
  <div class="form-group row">
	<div class="col-4">
          <span id="apellido1">Apellido 1</span>
  	<?= form_input('apellido1',set_value('apellido1'),['placeholder'=>" Primer Apellido", 'required'=>"required", 'class'=>"form-control" ])?>
	</div>
  </div>
 
 
 
  <div class="form-group row">
	<div class="col-4">
            <span id="apellido2">Apellido 2</span>
        <?= form_input('apellido2',set_value('apellido2'),['placeholder'=>"Segundo Apellido",'class'=>"form-control" ])?>
	</div>
  </div>
 



 <div class="form-group row">
	<div class="col-4">
            <span id="nacimiento">Fecha de nacimiento</span>
	<?= form_input('nacimiento',set_value('nacimiento'),['placeholder'=>"Fecha de Nacimiento",  'class'=>"form-control" ])?>

	</div>
  </div> 
 
  <div class="form-group row">
	<div class="col-4">
            	<span id="email">Dirección email</span>
<?= form_input('email',set_value('email'),['placeholder'=>"Email",  'class'=>"form-control" ])?>
 
	</div>
  </div>
  <div class="boton">
	<div>
  	<?= form_submit('boton','Enviar',['class'=>"btn btn-primary"]) ?>
	</div>

    
</html>