<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap5.css"/>
 
<script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap5.js"></script>

<script>
    
$(document).ready(function() {
   $('#table_id').dataTable( {
       "language": {
           "info": "",
           "lengthMenu": "Mostrar _MENU_ por página",
           "paginate": {
               "next": "Siguiente",
               "previous": "Anterior"
           },
           "search": "Busca un alumno:",
       }
   } );
} );
</script>
    <body class="bg-secondary">
        <nav class="navbar navbar-expand-sm bg-dark fixed-top">
            <div class="container-fluid">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="http://localhost:8080/codeigniter/index.php/homeController">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://localhost:8080/codeigniter/index.php/alumnesController">Listado Alumnos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://localhost:8080/codeigniter/index.php/gruposController/">Listado Grupos</a>
                    </li>
                </ul>
            </div>
        </nav>
        <br>
        <br>    
        <br>  

        <style>
             table, th, tr, td {
                border: solid 0.5px #BADED3;    
                border-collapse: collapse;
                text-align: center;
            }
            .container-fluid{
                margin-left: 7%;
                margin-right: 7%;
            }
            .hv:hover {
                background-color: #8288DF;
                color:white;
                font-weight: bold;
                transform: scale(1.05);
            }
            th{
                font-size: 24px;
                background-color: #5359B6;
                color:white;
            }
            tr {
                transition: background-color .5s, font-weight .2s;
                font-weight: normal;
            }
            td{
                width: 75px;
                font-size: 18px;
            }
            tr:nth-child(odd){
                background-color: #DCFCFF;
                transition: background-color .5s
            }
            .nav-link{
                color:white;
            }

        </style>

        <a href="http://localhost:8080/codeigniter/index.php/formularioController/" target="_blank"><button class="btn btn-success">
                Añadir Alumnos
            </button></a> 
        <table id="table_id" style="width:85%; margin: auto; background-color: #BAF2F6")>
            <thead>
                <tr>
                    <th>Foto</th> 

                    <th>NIA</th> 

                    <th>Nombre</th> 

                    <th>Apellido1</th> 

                    <th>Apellido2</th> 
                   
                    <th>NIF</th>
                    
                    <th>Fecha de Nacimiento</th>

                    <th>Email</th> 

                    <th>Grupo</th>

                    <th>Acciones</th>

                </tr>
            </thead>


            <?php foreach ($alumnos as $alumno): ?>
                <html>
                    <body>
                    <tr class="hv">
                        <td> <img style="width: 75px" src= "<?= base_url('imagenes/' . sprintf('%06s', $alumno['id'])) ?>.jpg">   </td>
                        <td><?= $alumno['NIA'] ?></td>
                        <td><?= $alumno['nombre'] ?></td>
                        <td><?= $alumno['apellido1'] ?></td>
                        <td><?= $alumno['apellido2'] ?></td>
                        <td><?= $alumno['nif'] ?></td>
                        <td><?= $alumno['fecha_nac'] ?></td> 
                        <td><?= $alumno['email'] ?> </td>
                        <td><?= $alumno['grupo'] ?></td>
                        <td><a href="<?= site_url('borrarController/eliminar/' . $alumno['NIA']) ?>" onclick = "return confirm('¿Estás seguro? Vas a eliminar al alumno');"><button type = "submit" class = "btn btn-danger" style = "margin-left:1%;">Borrar</button></td></a>
                    </tr>
                    </body>
                </html>
            <?php endforeach; ?>
        </table>
</html>
