<html lang="ES">
    
<head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
        
     <style>
            /*.container {
                    position: relative;
                    top: 25px;
                    left: 25px;
                    margin: auto;
                    transform: translate(-50%, -50%);
            }*/

            body {  
                    max-width: 100%;
                    height: 100%;
                    margin: 0 auto;
                    background: #6C757D;
                    background-repeat: no-repeat !important;
                    background-attachment: fixed;                
                    font-family: sans-serif;
                    font-weight: 100;
            }

            table {
                    width: 70%;
                    border-collapse: collapse;
                    overflow: hidden;
                    box-shadow: 0 0 20px rgba(0,0,0,0.1);
            }

            caption {
                    padding: 15px;
                    background-color: rgba(100, 40, 155, 0.25);
                    color: #fff;
            }

            th,
            td {
                    padding: 15px;
                    background-color: rgba(255,255,255,0.2);
                    color: #fff;
            }

            th {
                    text-align: left;
            }

            thead {
                    th {
                            background-color: #55608f;
                    }
            }

            .hover:hover {
                background-color: rgba(100, 40, 155, 0.25);
                color: white;
            }
            
            .age{
                color: red;
            }
            
            form{
                
                width:500px;
                padding:16px;
                border-radius:10px;
                margin-top: 150px;
                margin-left: 600px;
                background-color:#ccc;
            }

            form label{
                width:72px;
                font-weight:bold;
                display:inline-block;
            }

            form input[type="text"],
            form input[type="email"]{
                width:303px;
                padding:3px 10px;
                border:1px solid #f6f6f6;
                border-radius:3px;
                background-color:#f6f6f6;
                margin:8px 0;
                display:inline-block;
            }

            form input[type="submit"]{
                width:100%;
                padding:8px 16px;
                margin-top:32px;
                border:1px solid #000;
                border-radius:5px;
                display:block;
                color:#fff;
                background-color:#000;
            } 

            form input[type="submit"]:hover{
                cursor:pointer;
            }

            textarea{
                width:100%;
                height:100px;
                border:1px solid #f6f6f6;
                border-radius:3px;
                background-color:#f6f6f6;			
                margin:8px 0;
                /*resize: vertical | horizontal | none | both*/
                resize:none;
                display:block;
            }

            
        </style>
            
</head>
    
<body>
    
    <form action="<?= site_url('editorController/vistaFormularioEdit/'.$alumno['id'])?>" method="post">
        <div class="form-group row">
            <label for="text1" class="col-4 col-form-label">NIA</label> 
            <div class="col-8">
                <input id="text1" name="NIA"" type="text" size="8" maxlength="8" class="form-control" value="<?= $alumno['NIA']?>">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="text3" class="col-4 col-form-label">Nombre</label> 
            <div class="col-8">
                <input id="text3" name="nombre" type="text" class="form-control" value="<?= $alumno['nombre']?>">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="text4" class="col-4 col-form-label">Apellido1</label> 
            <div class="col-8">
                <input id="text4" name="apellido1" type="text" class="form-control" value="<?= $alumno['apellido1']?>">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="text" class="col-4 col-form-label">Apellido2</label> 
            <div class="col-8">
                <input id="text" name="apellido2" type="text" class="form-control" value="<?= $alumno['apellido2']?>">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="text2" class="col-4 col-form-label">Fecha de Nacimiento</label> 
            <div class="col-8">
                <input id="text2" name="fecha_nac" type="date" class="form-control" value="<?= $alumno['fecha_nac']?>">
            </div>
        </div> 
        
        <div class="form-group row">
            <label for="text2" class="col-4 col-form-label">NIF</label> 
            <div class="col-8">
                <input id="text2" name="nif" type="text" size="9" maxlength="9" class="form-control" value="<?= $alumno['nif']?>">
            </div>
        </div> 
        
        <div class="form-group row">
            <label for="text2" class="col-4 col-form-label">Email</label> 
            <div class="col-8">
                <input id="text2" name="email" type="email" class="form-control" value="<?= $alumno['email']?>">
            </div>
        </div> 
        
        <div class="form-group row">
            <div class="offset-4 col-8">
                <button name="submit" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
    
   
</body>
    



