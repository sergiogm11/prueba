<?php

namespace App\Controllers;

use CodeIgniter\Controller ;
use App\Libraries\mipdf;

class PdfController extends Controller {
    
    function index(){
        echo view ('vistaFormularioPDF.php');
    }
    
    function load(){
        $avatar = $this->request->getFile('file');
        $avatar->move(WRITEPATH . 'uploads', 'cars.csv', true);
        $this-> procesar();
    }
    
    function procesar(){
        $data['pdf']= new mipdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $file = fopen("../writable/uploads/cars.csv", "r");
        $i = 0;
        $numberOfFields = 5;
        
        $csvArr = array();
        
        while (($filedata = fgetcsv($file, 1000,","))!== FALSE) {
            $num = count($filedata);
            
            if ($i > 0 AND $num == $numberOfFields){
                $csvArr[$i]['nombre'] = $filedata[0];
                $csvArr[$i]['apellido'] = $filedata[1];
                $csvArr[$i]['email'] = $filedata[2];
                $csvArr[$i]['edad'] = $filedata[3];
                $csvArr[$i]['NIA'] = $filedata[4];
            }
            $i++;
        }
        $data['usuarios' ] = $csvArr;
        echo view('vistaPDF',$data);
    }
}
