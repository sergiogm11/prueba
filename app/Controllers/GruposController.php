<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\GruposModel;

class GruposController extends Controller {
    public function index()
    {
        $grupos = new GruposModel();
        $Vgrupos['titol'] = "Listado de Grupos";
        $Vgrupos['grupos'] = $grupos->findAll();
        echo view('Vgrupos', $Vgrupos);
    }
   
}


