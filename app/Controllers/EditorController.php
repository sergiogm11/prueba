<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\AlumnesModel;
use App\Models\FormularioModel;


class EditorController extends BaseController{
       
    public function vistaFormulario($id){
        $alumnos = new AlumnesModel();
        $alumnos = 
                $alumnos->SELECT('alumnos.id, alumnos.NIA, alumnos.nombre, apellido1, apellido2, email, fecha_nac, alumnos.nif')
                        ->where(['id'=>$id]) 
                        ->findAll();   
        $data['alumno'] = $alumnos[0];
        echo view('vistaFormulario', $data);
        
    }
    public function vistaFormularioEdit($id){
        $alumnos = new FormularioModel();
        $datos_nuevos = [
            'NIA' => $this->request->getPost('NIA'),
            'nombre' => $this->request->getPost('nombre'),
            'apellido1' => $this->request->getPost('apellido1'),
             'apellido2' => $this->request->getPost('apellido2'),
            'fecha_nac' => $this->request->getPost('fecha_nac'),
             'nif' => $this->request->getPost('nif'),
             'email' => $this->request->getPost('email')

       ];

        $alumnos->update($id, $datos_nuevos);
       
        header('Location:' . site_url('alumnesController'));
        exit();
    }
}


