<?php

namespace App\Controllers;

use App\Models\AlumnesModel;

class AlumnesController extends BaseController {

    public function index() {
        
        $alumnos = new AlumnesModel();

        $Valumnes2['alumnos'] = $alumnos->findAll();

        echo view('Valumnes2', $Valumnes2);
    }

    public function alumnosGrupo($valor ="") {
        $alumnos = new AlumnesModel();
        $titulo['titulo'] = "LISTADO DE ALUMNOS DE $valor";
        $Valumnes['alumnos'] = $alumnos->SELECT("alumnos.nombre ,alumnos.apellido1, alumnos.apellido2, email, alumnos.id, matricula.grupo, alumnos.NIA, alumnos.nif, alumnos.fecha_nac")
                ->join('matricula', 'alumnos.NIA = matricula.NIA', 'LEFT')
                ->where('grupo', $valor)
                ->findAll();

        echo view('Valumnes', $Valumnes);
        //echo view('datos', $datos);
    }

}


