<?php

namespace App\Models;

use CodeIgniter\Model;

class AlumnesModel extends Model {
    
    protected $table = 'alumnos';
    protected $allowedFields = ['NIA','nombre','apellido1','apellido2','id','fecha_nac','nif','email'];
    protected $validationRules    = [
        'NIA' => 'required|max_length[8]|min_length[8]|is_unique[alumnos.NIA]|is_natural',
        'nombre'     => 'required|min_length[1]',
        'apellido1'     => 'required|min_length[1]',
        'apellido2' => 'permit_empty',
        'nif' => 'required|max_length[9]|regex_match[/^[0-9XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKE]$/]|is_unique[alumnos.nif]',
        'email' => 'required|valid_email'
  ];
}

