<?php

namespace App\Models;

use CodeIgniter\Model;

class FormularioModel extends Model {

    protected $table = 'alumnos';
    protected $allowedFields = ['NIA','nombre','apellido1','apellido2','id','fecha_nac','nif','email'];
}